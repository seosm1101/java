package com.yedam.opp;

public class Aplication10 {
	public static void main(String[] args) {
		Student std1=new Student("김또치","예담고등학교",221124);
		Student std2=new Student("이또치","예담고등학교",221125);
		Student std3=new Student("박또치","예담고등학교",221126);
		Student std4=new Student("최또치","예담고등학교",221127);
		Student std5=new Student("정또치","예담고등학교",221128);
		Student std6=new Student("장또치","예담고등학교",221129);
		//배열에 넣어서 써보기
		//int intAry[]=new int[6];
		//intAry[0]=1;
		Student stdAry[]=new Student[6];
		stdAry[0]=std1;
		stdAry[1]=std2;
		stdAry[2]=std3;
		stdAry[3]=std4;
		stdAry[4]=std5;
		stdAry[5]=std6;
		
		for(int i=0;i<stdAry.length;i++) {
			stdAry[i].kor=50;
			stdAry[i].math=60;
			stdAry[i].eng=100;
			stdAry[i].getInfo();
		}
		
		
	}
}
