package com.yedam.opp;

import sun.java2d.opengl.WGLSurfaceData.WGLVSyncOffScreenSurfaceData;

public class Student {
	String name;
	String schoolName;
	int num;
	int kor;
	int math;
	int eng;
	
	public Student() {
		
	}
	public Student(String name, String schoolName, int num) {
		this.name=name;
		this.schoolName=schoolName;
		this.num=num;

	}
	
	int sumScore() {
		return kor+math+eng;
	}
	
	double avgScore() {
		double avg=sumScore()/(double)3;
		return avg;
	}
	
	void getInfo() {
		 System.out.println("학생의 이름 : "+name);
		 System.out.println("학생의 학교 : "+schoolName);
		 System.out.println("학생의 학번 : "+num);
		 System.out.println("총     점 : "+sumScore());
		 System.out.println("평     균 : "+avgScore());
		 System.out.println();
		 
//		 /Student[] stdAry=new Student[6];
	}
}
