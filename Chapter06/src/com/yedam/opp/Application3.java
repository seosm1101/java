package com.yedam.opp;

public class Application3 {
	public static void main(String[] args) {
		Book book1=new Book("혼자 공부하는 자바", 24000,"한빛미디어","yedam-001");
		Book book2=new Book("이것이 리눅스다", 32000,"한빛미디어","yedam-002");
		Book book3=new Book("자바스크립트 파워북",22000,"어포스트","yedam-003");
		
		
		book1.getInfo();
		book2.getInfo();
		book3.getInfo();
		
		
		
		
		Student stu1=new Student();
		stu1.name="고길동";
		stu1.schoolName="예담고등학교";
		stu1.num=221124;
		stu1.kor=50;
		stu1.math=100;
		stu1.eng=60;
		Student stu2=new Student("김둘리","예담고등학교",221125,100,80,85);
		Student stu3=new Student("김또치","예담고등학교",221126,90,60,70);
		
		
		
		stu1.getInfo();
		stu2.getInfo();
		stu3.getInfo();
		
		
		
		
	
	
	
	}
}
