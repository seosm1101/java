package com.yedam.opp;

public class Book {
	String name;
	String type="학습서";
	int price;
	String bookPub;
	String isbn;
	
	public Book(String name, int price, String bookPub, String isbn ) {
		this.name=name;
		this.price=price;
		this.bookPub=bookPub;
		this.isbn=isbn;
	}
	
	void getInfo() {
		System.out.println("책이름 : "+name);
		System.out.println("# 내용");
		System.out.println("1)종류 : "+type);
		System.out.println("2)가격 : "+price);
		System.out.println("3)출판사 : "+bookPub);
		System.out.println("4)도서번호 : "+isbn);
		System.out.println();
	}
}
