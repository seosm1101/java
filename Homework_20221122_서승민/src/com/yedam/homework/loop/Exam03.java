package com.yedam.homework.loop;

import java.util.Scanner;

public class Exam03 {
	public static void main(String[] args) {
		boolean flag=true;
		int chance=5;
		int value=(int)((Math.random()*99)+1);
		System.out.println(value);
		Scanner sc =new Scanner(System.in);
		
		while(flag) {
			System.out.println(chance+"번 남았습니다.");
			
			System.out.println("숫자를 입력하세요 : ");
			int inputValue=Integer.parseInt(sc.nextLine());
			
			if(inputValue==value) {
				System.out.println("정답 : "+value);
				System.out.println("축하합니다.");
				break;
			}else if(inputValue!=value) {
				if(inputValue<value) {
					System.out.println("up");
				}else if(inputValue>value) {
					System.out.println("down");
				}
			}
			chance=chance-1;
			if(chance==0) {
				System.out.println("모든 기회를 사용하셨습니다.");
				break;
			}
		}
	}
}
