package com.yedam.homework.loop;

import java.util.Scanner;

public class Exam01 {
	public static void main(String[] args) {
		Scanner sc =new Scanner(System.in);
		
		System.out.println("x,y값을 입력하세요.");
		System.out.println("x>");
		int x=Integer.parseInt(sc.nextLine());
		System.out.println("y>");
		int y=Integer.parseInt(sc.nextLine());
		
		if(x>0&&y>0) {
			System.out.println("제1사분면");
		}else if(x<0&&y>0) {
			System.out.println("제2사분면");
		}else if(x<0&&y<0) {
			System.out.println("제3사분면");
		}else if(x>0&&y<0) {
			System.out.println("제4사분면");
		}
		
	}
}
