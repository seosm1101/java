package com.yedam.homework.loop;

import java.util.Scanner;

public class Exam02 {
	public static void main(String[] args) {
		Scanner sc =new Scanner(System.in);
		System.out.println("연도를 입력하세요 : ");
		int year=Integer.parseInt(sc.nextLine());
		
		if(year%4==0) {
			if(year%100!=0) {
				System.out.println("윤년");
			}else if(year%400==0) {
				System.out.println("윤년");
			}else {
				System.out.println("윤년이 아닙니다.");
			}
		}else {
			System.out.println("윤년이 아닙니다.");
		}
		
		
	}
}
