package com.yedam.homework_;

public class Exam01 {
	public static void main(String[] args) {
		int x=-5;
		int y=10;
		int result;
		
		result=-x;
		System.out.printf("x: %d, result: %d\n",x,result);
		
		result=++x+y--;
		System.out.printf("x: %d, y: %d, result: %d\n",x,y,result);
		
		result=x+y;
		System.out.printf("x: %d, y: %d, result: %d\n",x,y,result);
	}
}
