package com.yedam.homework_;

public class Exam02 {
	public static void main(String[] args) {
		int m=10;
		int n=5;
		boolean valReverse1=!((m*2==n*4)||(n<=5));
		boolean valReverse2=!((m/2>5)&&(n%2<1));
		
		System.out.println((m*2==n*4)||(n<=5));
		System.out.println(valReverse1);
		System.out.println((m/2>5)&&(n%2<1));
		System.out.println(valReverse2);
	}
}
