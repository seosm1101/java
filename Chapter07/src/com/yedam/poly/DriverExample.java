package com.yedam.poly;

public class DriverExample {
	public static void main(String[] args) {
		//Vehicle=>Bus,taxi
		//Driver=>vehicle 매개 변수로 하는 drive
		//drive(Vehicle vehicle) <-매개 변수에 자식 클래스를 대입
		
		Vehicle vehicle=new Vehicle();
		
		Driver driver=new Driver();
//		driver.drive(null);
		//Bus bus=new Bus();
		//driver.drive(bus);
		driver.drive(new Bus());
		
		driver.drive(new taxi());
	}
}
