package com.yedam.access;

import com.yedam.inheritance.A;

public class D extends A {
	public D() {
		//부모생성자 호출 & 객체 생성
		super();
		//부모 필드 접군
		this.field="value";
		//부모 메소드 접근
		this.method();
	}
}
