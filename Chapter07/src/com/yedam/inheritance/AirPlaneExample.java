package com.yedam.inheritance;

public class AirPlaneExample {
	public static void main(String[] args) {
		SuperSonicAirPlane sa=new SuperSonicAirPlane();
		
		sa.takeOff();
		
		sa.fly();
		
		sa.flyMode=SuperSonicAirPlane.SUPERSONIC;
		
		sa.fly();
		
		sa.flyMode=SuperSonicAirPlane.NORMAR;
		
		sa.fly();
		
		sa.land();
	}
}
