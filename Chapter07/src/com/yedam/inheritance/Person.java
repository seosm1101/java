package com.yedam.inheritance;

public class Person extends People {
	
	public int age;
	//자식 객체를 만들때, 생성자를 통해서 만든다.
	//super()를 통해서 부모 객체를 생성한다.
	//여기서 super() 의마히는 것은 부모의 생성자를 호출
	//따라서 자식 객체를 만들게 되면
	public Person(String name,String ssn) {
		super(name,ssn);
	}
}
