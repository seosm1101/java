package com.yedam.inheritance;

public class Application {
	public static void main(String[] args) {
		Child child=new Child();
		
		child.lastName="또치";
		child.age=20;
		System.out.println("내 이름 : "+child.firstName+child.lastName);
		System.out.println("DNA : "+child.DNA);
		//Parent 클래스->bloodType을 private 설정
		//Child 클래스->parent 클래스의 bloodType 사용x
//		System.out.println("혈액형 : "+child.bloodType);
		System.out.println("나이 "+child.age);
		System.out.println();
		
		Child2 child2=new Child2();
		
		child2.lastName="희동";
		child2.age=5;
		child2.bloodType='A';
		
		System.out.println("내 이름 : "+child2.firstName+child2.lastName);
		System.out.println("DNA : "+child2.DNA);
		//Child에 존재하는 bloodType ->상관x
		System.out.println("혈액형 : "+child2.bloodType);
		System.out.println("나이 "+child2.age);
		
	}
}
