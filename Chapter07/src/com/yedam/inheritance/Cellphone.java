package com.yedam.inheritance;

public class Cellphone {
	public String model;
	public String color;
	
	void powerON() {
		System.out.println("전원을 켬");
	}
	void powerOff() {
		System.out.println("전원을 끔");
	}
	void bell() {
		System.out.println("전화를 검.");
	}
	void hangUp() {
		System.out.println("전화를 끊음.");
	}
}
