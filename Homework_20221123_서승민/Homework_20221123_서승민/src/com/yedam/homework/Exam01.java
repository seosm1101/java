package com.yedam.homework;

import java.util.Arrays;
import java.util.Scanner;

public class Exam01 {
	public static void main(String[] args) {
		int arr1[]= {10,20,30,70,3,60,-3};
		
		//문제1. 주어진 배열 중에서 값이 60인 곳의 인덱스를 출력해보자.
		for(int i=0;i<arr1.length;i++) {
			if(arr1[i]==60) {
				System.out.println("값이 60인 곳의 인덱스 : "+i);
			}
		}
		
		//문제2. 주어진 배열의 인덱스가 3인 곳은 출력하지 말고,
		//나머지만 출력해보자.
		for(int i=0;i<arr1.length;i++) {
			if(i==3) {
				
			}else {
				System.out.print(arr1[i]+"\t");
			}
		}
		System.out.println();
		
		//문제3. 주어진 배열 안의 변경하고 싶은 값의 인덱스 번호를 입력받아,
		//그 값을 1000으로 변경해보자.
		Scanner sc=new Scanner(System.in);
		System.out.println("변경하고 싶은 값의 인덱스 번호: ");
		int arrChangeNo=Integer.parseInt(sc.nextLine());
		
		for(int i=0;i<arr1.length;i++) {
			if(i==arrChangeNo) {
				arr1[i]=1000;
			}
			System.out.print(arr1[i]+"\t");
		}
		System.out.println();
		
		//문제4. 주어진 배열의 요소에서 최대값과 최소값을 구해보자.
		int max=arr1[0];
		int min=arr1[0];
		for(int i=0;i<arr1.length;i++) {
			if(max<arr1[i]) {
				max=arr1[i];
			}
			if(min>arr1[i]) {
				min=arr1[i];
			}
		}
		System.out.println("최대값 : "+max);
		System.out.println("최소값 : "+min);
		
		//문제5. 별도의 배열을 선언하여 양의 정수 10개를 입력받아 배열에 저장하고,
		//배열에 있는 정수 중에서 3의 배수만 출력해보자.
		int arr2[]=new int[10];
		System.out.println("양의 정수 10개를 입력하세요");
		for(int i=0;i<arr2.length;i++) {
			System.out.println((i+1)+"번째 정수 입력>");
			int inputVal=Integer.parseInt(sc.nextLine());
			arr2[i]=inputVal;
		}
		for(int i=0;i<arr2.length;i++) {
			if(arr2[i]%3==0) {
				System.out.print(arr2[i]+"\t");
			}
		}
		System.out.println();
		
		//추가문제 - Lotto 생성
		//1)1~45사이의 수를 랜덤으로 추출합니다.
		//2)랜덤으로 추출한 번호는 중복이 되선 안됩니다.
		//3)번호는 6개만 추출합니다.
		int lottoArr[]=new int[6];
		
		for(int i=0;i<lottoArr.length;i++) {
			lottoArr[i]=(int)(Math.random()*45)+1;
			
			for(int j=0;j<i;j++) {
					
				while(true) {
					if(lottoArr[i]==lottoArr[j]) {
						lottoArr[i]=(int)(Math.random()*45)+1;	
					}
					if(lottoArr[i]!=lottoArr[j]) {
						break;
					}
				}
			
			}
		}
		Arrays.sort(lottoArr);
		for(int i=0;i<lottoArr.length;i++) {
			System.out.print(lottoArr[i]+"\t");	
		}
		
	}
}
