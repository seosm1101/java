package com.yedam.inter;

public class RCExample {
	public static void main(String[] args) {
		RemoteControl rc;
		
		rc=new SmartTV();
		
		//SmartTV 클래스=>implements RemoteControl(+Searchable)
		//RemoteControl->Searchable ->searchable을 상속받고 있기 때문에
		//RemoteControl
		rc.turnOn();
		rc.setVolume(40);
		rc.turnOff();
		//Searchable
		rc.search("www.google.com");
//		Searchable sc=new SmartTV();
//		sc.search("wwww.google.com");
		
		rc=new Audio();
		
		rc.turnOn();
		rc.setVolume(5);
		rc.turnOff();
		
//		Television tv=new Television();
		
		
	}
}
