package com.yedam.study;

import java.util.Scanner;

public class Application {
	public static void main(String[] args) {
		//두 개의 주사위를 던졌을때, 눈의 합이 6이 되는 모든 경우의 수를
		//출력하는 프로그램 구현을 하시오.
		//1,5
		//2,4
		//3,3
		//4,2
		//5,1
		
		for(int i=1;i<=6;i++) {
			for(int j=1;j<=6;j++) {
				if(i+j==6) {
					System.out.println("1번주사위:"+i+" 2번주사위:"+j);
				}
			}
		}
		
		//숫자를 하나 입력 받아, 양수인지 음수인지 출력
		//단 0이면 0입니다라고 출력해주세요.
		System.out.println("숫자를 입력하세요 : ");
		Scanner sc=new Scanner(System.in);
		int number=Integer.parseInt(sc.nextLine());
		if(number>0) {
			System.out.println("양수");
		}else if(number==0){
			System.out.println("0입니다");
		}else {
			System.out.println("음수");
		}
		
		
		//정수 두개와 연산기호 1개를 입력 받아서
		//연산 기호에 해당되는 계산을 수행하고 출력하세요.
		System.out.println("정수 두개를 입력하세요");
		System.out.println("첫번째 정수");
		int number1=Integer.parseInt(sc.nextLine());
		System.out.println("두번째 정수");
		int number2=Integer.parseInt(sc.nextLine());
		System.out.println("부호를 입력하세요>");
		String sign=sc.nextLine();
		
		switch(sign) {
		case "+" :
			System.out.println(number1+number2);
			break;
		case "-" :
			System.out.println(number1-number2);
			break;
		case "*" :
			System.out.println(number1*number2);
			break;
		case "/" :
			System.out.println(number1/number2);
			break;
		default:
			System.out.println("잘못입력하셨습니다");
			break;
		}
		
		if(sign.equals("+")) {
			System.out.println(number1+number2);
		}else if(sign.equals("-")) {
			System.out.println(number1-number2);
		}else if(sign.equals("*")) {
			System.out.println(number1*number2);
		}else if(sign.equals("/")) {
			System.out.println(number1/number2);
		}else {
			System.out.println("잘못입력하셨습니다");
		}
		
		
		
		
		
	}
}
