package com.yedam.test;

import java.util.Scanner;

public class Test05 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		boolean flag=true;
		int diceNo=0;
		int diceValue=0;
		int diceSize=0;
		int num[]=null;
		
		while(flag) {
			System.out.println("===1.주사위 크기 2.주사위 굴리기 3.결과 보기 4.가장 많이 나온 수 5.종료===");
			System.out.println("메뉴 >");
			diceNo=Integer.parseInt(sc.nextLine());
			switch(diceNo) {
			case 1:
				System.out.println("주사위 크기 >");
				diceSize=Integer.parseInt(sc.nextLine());
				if(5<=diceSize&&diceSize<=10) {
					diceValue=(int)((Math.random()*diceSize)+1);
				}else {
					System.out.println("5~10사이의 정수를 입력하세요.");
				}
				break;
			case 2:
				int count=0;
				num =new int[diceSize+1];
				while(true) {
					for(int i=1;i<=diceSize;i++) {
						if(diceValue==i) {
							num[i]++;
						}
					}
					if(diceValue!=5) {
						diceValue=(int)((Math.random()*diceSize)+1);
						count++;
					}else if(diceValue==5) {
						break;
					}
				}
				System.out.println("5가 나올 때까지 주사위를 "+count+"번 굴렸습니다.");
				break;
				
//				while(true) {
//					int random=(int)(Math.random()*diceSize)+1;
//					num[random-1]=num[random-1]+1;
//					count++;
//					if(random==5) {
//						break;
//					}
//				}
				
			case 3:
				for(int i=1;i<=diceSize;i++) {
					System.out.println(i+"은"+num[i]+"번 나왔습니다.");
				}
				break;
			case 4:
				int max=num[1];
				for(int i=1;i<=diceSize;i++) {
					if(num[i]>max) {
						max=i;
					}
				}
				System.out.println("가장 많이 나온 수는 "+max+"입니다.");
				break;
			case 5:
				System.out.println("프로그램 종료");
				flag=false;
				break;
			default:
				System.out.println("잘못입력하셨습니다.");
				break;
			}
		}
	}
}
