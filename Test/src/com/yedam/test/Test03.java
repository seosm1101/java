package com.yedam.test;

public class Test03 {
	public static void main(String[] args) {
		int num1=10;
		int num2=2;
		char operator='+';
		
		switch(operator) {
		case '+':
			System.out.println("두수의 '+' 연산 결과값:"+(num1+num2));
			break;
		case '-':
			System.out.println("두수의 '-' 연산 결과값:"+(num1-num2));
			break;
		case '*':
			System.out.println("두수의 '*' 연산 결과값:"+(num1*num2));
			break;
		case '/':
			System.out.println("두수의 '/' 연산 결과값:"+(num1/num2));
			break;
		default :
			System.out.println("잘못입력하셨습니다.");
			break;
		}
	}
}
