package com.yedam.test;

public class Student {
	
	private String stdName;
	private String stdSub;
	private String stdGrade;
	private int programing;
	private int dataBase;
	private int OS;
	
	//필드에 대한 데이터를 객체를 생성할 때 초기화 할 예정이라면
	//생성자에 this 키워드를 활용해서 필드초기화 하면됨
	public Student(){
		
	}
	
	
	//우측클릭 souce-getter and setter
	public String getstdName() {
		return stdName;
	}
	public void setstdName(String name) {
		this.stdName=name;
	}
	
	public String getstdSub() {
		return stdSub;
	}
	public void setstdSub(String sub) {
		this.stdSub=sub;
	}

	public String getStdGrade() {
		return stdGrade;
	}
	public void setStdGrade(String stdGrade) {
		this.stdGrade = stdGrade;
	}

	public int getPrograming() {
		return programing;
	}
	public void setPrograming(int programing) {
		if(programing<=0) {
			this.programing=0;
		}
		this.programing = programing;
	}

	public int getDataBase() {
		return dataBase;
	}
	public void setDataBase(int dataBase) {
		if(dataBase<=0) {
			this.dataBase=0;
		}
		this.dataBase = dataBase;
	}

	public int getOS() {
		return OS;
	}
	public void setOS(int oS) {
		if(oS<=0) {
			this.OS=0;
		}
		OS = oS;
	}
	
	
}
