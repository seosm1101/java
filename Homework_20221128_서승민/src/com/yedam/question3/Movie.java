package com.yedam.question3;

public class Movie extends Culture {

	//필드
	public String gerne;
	
	
	
	//생성자
	public Movie(String title, int director, int actor, String gerne) {
		super(title, director, actor);
		this.gerne=gerne;
	}
	

	
	//메소드
	@Override
	public void getInformation() {
		System.out.println(gerne+"영화제목 : "+title);
		System.out.println(gerne+"감독 : "+director);
		System.out.println(gerne+"배우 : "+actor);
		System.out.println(gerne+"영화총점 : "+total);
		System.out.println(gerne+"영화평점 : "+getGrade());
	}
}
