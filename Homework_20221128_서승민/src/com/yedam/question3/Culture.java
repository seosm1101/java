package com.yedam.question3;

public abstract class Culture {
	//필드
	public String title;
	public int director;
	public int actor;
	public int audience;
	public int total;
	
	//생성자
	public Culture(String title, int director, int actor) {
		this.title=title;
		this.director=director;
		this.actor=actor;
	}
	
	//메소드
	public void setTotalScore(int score) {
		//관객 수 1씩 증가
		this.audience++;
		//점수를 누적(총점)
		this.total+=score;
	}
	
	public String getGrade() {
		int avg=total/audience;
		
		String grade=null;
		
		switch(avg) {
		case 1:
			grade="☆";
			break;
		case 2:
			grade="☆☆";
			break;
		case 3:
			grade="☆☆☆";
			break;
		case 4:
			grade="☆☆☆☆";
			break;
		case 5:
			grade="☆☆☆☆☆";
			break;
		}
		//2번째 방법
		//반복문
//		for(int i=0;i<avg;i++) {
//			grade+="☆";
//		}
	return grade;	
	}
	
	public abstract void getInformation();
	
}
