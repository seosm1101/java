package com.yedam.question2;

public class ObesityInfo extends StandardWeightInfo{
	//필드
	
	
	//생성자
	public ObesityInfo(String name, double height, double weight) {
		super(name, height, weight);
	}
	
	//메소드
	@Override
	public void getInformation() {
		String obesity =null;
		
		double bmi=getObesity();
		if(bmi<=18.5) {
			obesity="저체중";
		}else if(bmi<=22.9) {
			obesity="정상";
		}else if(bmi<=24.9) {
			obesity="과체중";
		}else{
			obesity="비만";
		}

		System.out.println(name+"님의 신장 "+(int)height+", 몸무게 "+(int)weight+" "+obesity);
		
	}
	public double getObesity() {
		double bmi=(weight-getStandardWeight())/getStandardWeight()*100;
		
		return bmi;
	}
}


