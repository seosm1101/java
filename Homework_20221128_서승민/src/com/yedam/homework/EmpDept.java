package com.yedam.homework;

public class EmpDept extends Employee {
	//필드
	public String department;
	
	
	//생성자
	public EmpDept(String name, String slalary, String department) {
		super(name, slalary);
		this.department=department;
	}


	//메소드
	public String getDepartment() {
		return department;
	}


	
	@Override
	public void getInformation() {
		super.getInformation();
		System.out.println(" 부서 : "+department);
	}



	@Override
	public void print() {
		super.print();
		System.out.println("서브클래스");
	}
	
	
}
